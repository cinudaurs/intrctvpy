# template for "Guess the number" mini-project
# input will come from buttons and an input field
# all output for the game will be printed in the console

import simplegui
import random
import math


# initialize global variables used in your code
secret_number = 0
guess_number = 0
range_number = 100
counter = 7




# helper function to start and restart the game
def new_game():
    global secret_number
    global range_number
    global counter
    print ""
    if range_number == 100:
        
        secret_number = range100()
        counter = 7
               
    elif range_number == 1000:
        
        secret_number = range1000()
        counter = 10
            
    return secret_number


# define event handlers for control panel
def range100():
    # button that changes range to range [0,100) and restarts
    global range_number
    global secret_number
    global counter
    counter = 7
    
    print 'New game. Range is from 0 to 100'
    print 'Number of remaining guesses is', counter
    print ""
    
    range_number = 100
    
    secret_number = random.randrange(0,100)
    
    return secret_number
          
    

def range1000():
    # button that changes range to range [0,1000) and restarts
    global secret_number
    global range_number
    global counter  
    counter = 10
    
    print 'New game. Range is from 0 to 1000'
    print 'Number of remaining guesses is', counter
    print ""
    
    range_number = 1000
    
        
    secret_number = random.randrange(0,1000)
    
    return secret_number
      
    
def input_guess(guess):
    # main game logic goes here	
    global guess_number
    global secret_number
    global range_number
    global counter
    guess_number = int(guess)
    
    print 'Guess was', guess_number
    
    if guess_number < secret_number:
        
        counter = counter - 1
        
        if counter != 0:
            print 'Number of remaining guesses is', counter
            print 'Higher!'
            print ""
            
        elif counter == 0:
            print 'Number of remaining guesses is', counter
            print "You ran out of guesses. Game Over! The Secret number was", secret_number
            new_game()
        
    elif guess_number > secret_number:
        
        counter = counter - 1
        
        if counter != 0:
            print 'Number of remaining guesses is', counter
            print 'Lower!'
            print ""

        
        elif counter == 0:
            print 'Number of remaining guesses is', counter
            print "You ran out of guesses. Game Over! The secret number was", secret_number
            new_game()
        
    else:
        
        counter = counter - 1
        print 'Number of remaining guesses is', counter
        print 'Correct!'
        new_game()
    
   
# create frame
window_name = "GuessTheNumber"
canvas_width = 200
canvas_height = 200
control_width = 200
f = simplegui.create_frame(window_name, canvas_width, canvas_height, control_width)


# register event handlers for control elements

f.add_button("Range is [0,100)", range100, 200)
f.add_button("Range is [0,1000)", range1000, 200)
f.add_input("Enter a guess", input_guess, 200)
# call new_game and start frame

new_game()
f.start()

