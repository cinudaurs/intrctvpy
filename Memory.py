# implementation of card game - Memory
import simplegui
import random

CARD_WIDTH = 50
CARD_HEIGHT = 100
HALF_CARD_WIDTH = CARD_WIDTH / 2
HALF_CARD_HEIGHT = CARD_HEIGHT / 2

card_pos = [HALF_CARD_WIDTH , HALF_CARD_HEIGHT] 
number_pos = [HALF_CARD_WIDTH , HALF_CARD_HEIGHT]

state = 0
card_list = range(8) + range(8)
exposed =  [False for i in range(0,16)]
card_index = 0
card1_index = 0
card2_index = 0
card3_index = 0
turns = 0

# helper function to initialize globals
def new_game():
    global state, exposed, card_index, card1_index, card2_index, card3_index, turns
    state = 0
    random.shuffle(card_list)
    exposed =  [False for i in range(0,16)]
    card_index = 0
    card1_index = 0
    card2_index = 0
    card3_index = 0
    turns = 0
      
# define event handlers
def mouseclick(pos):
    # add game state logic here
    global state, exposed, card1_index, card2_index, card3_index, turns
    card_index = pos[0]//50
    
    if exposed[card_index] == True:
                return    
               
    if state == 0:
        state = 1
        
        card1_index = card_index
        exposed[card1_index] = True
        
        
    elif state == 1:
         state = 2
                         
         card2_index = card_index
         exposed[card2_index] = True
         
    else:
         state = 1
         turns+=1        
         card3_index = card_index
         if card_list[card1_index] == card_list[card2_index]:
                pass
         else:
                exposed[card1_index] = False
                exposed[card2_index] = False
         
         card1_index = card3_index 
         exposed[card1_index] = True
                
         
         
   
                        
# cards are logically 50x100 pixels in size    
def draw(canvas):
   
   global state, turns, exposed
  
   number_pos = [CARD_WIDTH/2 , HALF_CARD_HEIGHT]
   card_pos = [HALF_CARD_WIDTH , HALF_CARD_HEIGHT]
   
   for card_index, card in enumerate(card_list):
             
        if exposed[card_index]:    
            
            canvas.draw_text(str(card), (CARD_WIDTH * (card_index+1) - HALF_CARD_WIDTH, number_pos[1]), 20, 'White')
                   
        else:    

            card_pos[0] = (HALF_CARD_WIDTH) * (2*card_index+1)
            canvas.draw_line([card_pos[0] ,card_pos[1] - HALF_CARD_HEIGHT], [card_pos[0] , card_pos[1] + HALF_CARD_HEIGHT], CARD_WIDTH-1, 'Orange')
            
   label.set_text("Turns = "+ str(turns))     
   
        
        
# create frame and add a button and labels
frame = simplegui.create_frame("Memory", 800, 100)
frame.add_button("Reset", new_game)
label = frame.add_label("Turns = 0")
# register event handlers
frame.set_mouseclick_handler(mouseclick)
frame.set_draw_handler(draw)
# get things rolling
new_game()
frame.start()
# Always remember to review the grading rubric