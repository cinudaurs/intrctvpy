# Mini-project #6 - Blackjack

import simplegui
import random

# load card sprite - 949x392 - source: jfitz.com
CARD_SIZE = (73, 98)
CARD_CENTER = (36.5, 49)
card_images = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/cards.jfitz.png")

CARD_BACK_SIZE = (71, 96)
CARD_BACK_CENTER = (35.5, 48)
card_back = simplegui.load_image("http://commondatastorage.googleapis.com/codeskulptor-assets/card_back.png")  

# initialize some useful global variables
in_play = False
outcome_plyr_messg = "Hit or Stand ?"
outcome_dealr_messg = ""

player_score = 0

dealer_hand = None
player_hand = None

is_dealr_hand = True

# define globals for cards
SUITS = ('C', 'S', 'H', 'D')
RANKS = ('A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K')
VALUES = {'A':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, 'T':10, 'J':10, 'Q':10, 'K':10}


dealer_first_card_pos = [75, 150]
player_first_card_pos = [75, 350]



# define card class
class Card:
    def __init__(self, suit, rank):
        if (suit in SUITS) and (rank in RANKS):
            self.suit = suit
            self.rank = rank
        else:
            self.suit = None
            self.rank = None
            print "Invalid card: ", suit, rank

    def __str__(self):
        return self.suit + self.rank

    def get_suit(self):
        return self.suit

    def get_rank(self):
        return self.rank

    def draw(self, canvas, pos):
        card_loc = (CARD_CENTER[0] + CARD_SIZE[0] * RANKS.index(self.rank),
                    CARD_CENTER[1] + CARD_SIZE[1] * SUITS.index(self.suit))
        
        canvas.draw_image(card_images, card_loc, CARD_SIZE, [pos[0] + CARD_CENTER[0], pos[1] + CARD_CENTER[1]], CARD_SIZE)
        
    def draw_back(self, canvas, pos):
        card_loc = (CARD_BACK_CENTER[0],CARD_BACK_CENTER[1])
        
        canvas.draw_image(card_back, card_loc, CARD_BACK_SIZE, [pos[0] + CARD_BACK_CENTER[0], pos[1] + CARD_BACK_CENTER[1]], CARD_BACK_SIZE)
        
        
      
# define hand class
      
class Hand:
  
    def __init__(self):
            # create Hand object
            self.hand_cards = []             
  
    def __str__(self):
          
            string = "Hand contains "
          
            # return a string representation of a hand
            if not self.hand_cards:
                    return string
            else:         
                for card in self.hand_cards:
                    string += str(card)+ " "
          
            return string

    def add_card(self, card):
            # add a card object to a hand
            return self.hand_cards.append(card)

    def get_value(self):
        # count aces as 1, if the hand has an ace,
        # then add 10 to hand value if it doesn't bust
        # compute the value of the hand, see Blackjack video
      
        for card in self.hand_cards:
            if not card.get_rank():
                return None
      
        hand_value = 0
      
        for i in self.hand_cards:
            hand_value+=VALUES[i.get_rank()]
           
        has_aces = False
      
        for card in self.hand_cards:
            if card.get_rank() == 'A':
                has_aces = True

        if not has_aces:
            return hand_value
        else:
            if hand_value + 10 <= 21:
                return hand_value + 10
            else:
                return hand_value
             
 
    def draw(self, canvas, pos):
        # draw a hand on the canvas, use the draw method for cards
        
        global is_dealr_hand, dealer_first_card_pos, player_first_card_pos
               
        if is_dealr_hand:
            pos = dealer_first_card_pos
            for index, card in enumerate(self.hand_cards):
                
                if index == 1 and in_play:
                    card.draw_back(canvas, pos)
                                 
                else:               
                    card.draw(canvas, pos)
                    
                pos[0]+=25
                
                is_dealr_hand = False
            
        else:
            pos = player_first_card_pos
            for card in self.hand_cards:
              card.draw(canvas, pos)
              pos[0]+=25
              is_dealr_hand = True
  
      
# define deck class
class Deck:
    def __init__(self):
        # create a Deck object
        self.deck_cards = []
      
        # create a Card object using Card(suit, rank)
        # and add it to the card list for the deck
        for suit in SUITS:
            for rank in RANKS:
                self.deck_cards.append(Card(suit,rank))
       
       
       
    def shuffle(self):
      
        # shuffle the deck
        # use random.shuffle()
        return random.shuffle(self.deck_cards)
      

    def deal_card(self):
        # deal a card object from the deck
         return self.deck_cards.pop()
      
      
  
    def __str__(self):
            # return a string representing the deck
            string = "Deck contains "
          
            # return a string representation of a hand
            if not self.deck_cards:
                    return string
            else:         
                for card in self.deck_cards:
                    string += str(card)+ " "
          
            return string

#define event handlers for buttons
def deal():
    global outcome_plyr_messg, outcome_dealr_messg, in_play, deck, player_hand, dealer_hand
    global dealer_score , player_score
    
    if in_play:
        outcome_plyr_messg = "You clicked Deal in the middle of a round, Player, you lose!"
        player_score-=1
        in_play = False
        return
    
    in_play = True
    outcome_plyr_messg = "Hit or Stand ?"
    outcome_dealr_messg = ""
           
    deck = Deck()
    player_hand = Hand()
    dealer_hand = Hand()   
   
    deck.shuffle()
   
   
    if deck.deck_cards:
       
       
        plyr_card1 = deck.deal_card()
        player_hand.add_card(plyr_card1)
       
        plyr_card2 = deck.deal_card()
        player_hand.add_card(plyr_card2)
       
        dealr_card1 = deck.deal_card()
        dealer_hand.add_card(dealr_card1)
       
        dealr_card2 = deck.deal_card()
        dealer_hand.add_card(dealr_card2)
        
               
        for plyr_hnd_card in player_hand.hand_cards:
            print "Player hand contains " + str(plyr_hnd_card)
            
           
        for dealr_hnd_card in dealer_hand.hand_cards:
            print "Dealer hand contains " + str(dealr_hnd_card)
       
       
    else:
        print "No Cards left!"

   
   
   
   

def hit():
    # replace with your code below
    global player_hand, deck, in_play, player_score, outcome_plyr_messg, outcome_dealr_messg
          
    if not in_play:
        if player_hand.get_value() > 21:
            print "Reminder : You have been busted!"
            outcome_plyr_messg = "Reminder : You have been busted! New Deal ?"
            in_play = False
    
    
    
    # if the hand is in play, hit the player
    if in_play:
        if player_hand.get_value() < 21:
            player_hand.add_card(deck.deal_card())
            print "Cards after hit"
            outcome_plyr_messg = "Hit or Stand ?"
            for card in player_hand.hand_cards:
                print str(card)
            
        if player_hand.get_value() > 21:
                print "Player,You went bust!"
                
                outcome_plyr_messg = "Player, You went bust!, New Deal ?"
                outcome_dealr_messg = "Dealer wins."
                
                in_play = False
                
                player_score-=1
                
       
        if player_hand.get_value() == 21:
                outcome_plyr_messg = "Player wins!, New Deal ?"
                
                in_play = False
                
                player_score+=1
 
   
     
def stand():
    # replace with your code below
    global dealer_hand, dealer_score, in_play, player_hand, player_score, outcome_dealr_messg, outcome_plyr_messg
    # if hand is in play,
    #repeatedly hit dealer until his hand has value 17 or more
    
    
      
    if not in_play:
        if player_hand.get_value() > 21:
            print "Reminder : You have been busted!"
            outcome_plyr_messg = "Reminder : You have been busted! New Deal ?"
            in_play = False
       
    
    if in_play:
       while dealer_hand.get_value() < 17:
            dealer_hand.add_card(deck.deal_card())
            print "Dealer hand contains"
            for card in dealer_hand.hand_cards:
                print str(card)
   
      # assign a message to outcome, update in_play and score
       if dealer_hand.get_value() > 21:
                print "Dealer, you've busted!"
                outcome_dealr_messg = "Dealer, you went bust!"
                outcome_plyr_messg = "Player, you win!, New Deal ?"
                
                in_play = False
                
                player_score+=1
                
       else:
          #compare the value of the player's and dealer's hands
          if player_hand.get_value() <= dealer_hand.get_value():
                        print "Dealer wins!"
                        
                        if player_hand.get_value() == dealer_hand.get_value():
                            outcome_dealr_messg = "Tie! Dealer wins."
                            outcome_plyr_messg = "New Deal?"
                        else:
                            outcome_dealr_messg = "Dealer wins."
                            outcome_plyr_messg = "New Deal?"
                        
                        in_play = False
                        player_score-=1
          else:
                        print "Player wins!"
                        outcome_plyr_messg = "Player, you win!, New Deal ?"
                        
                        in_play = False
                        player_score+=1
                       
               

# draw handler  
def draw(canvas):
    # test to make sure that card.draw works, replace with your code below
  
    global in_play, dealer_first_card_pos, player_first_card_pos, outcome_dealr_messg, outcome_plyr_messg, dealer_hand, player_hand
    global dealer_score, player_score
    
    canvas.draw_text("Blackjack", (230, 50 ), 35, 'DarkOrange')
    canvas.draw_text("Dealer", (75, 135), 20, 'BlanchedAlmond')
    canvas.draw_text("Player", (75, 335), 20, 'BlanchedAlmond')
    
    dealer_first_card_pos = [75, 150]
    dealer_hand.draw(canvas, dealer_first_card_pos)
        
    player_first_card_pos = [75, 350]
    player_hand.draw(canvas, player_first_card_pos)
    
    canvas.draw_text(outcome_dealr_messg, (100, 275), 20, 'BlanchedAlmond')
    canvas.draw_text(outcome_plyr_messg, (100, 475), 20, 'BlanchedAlmond')
    
    canvas.draw_text("Player Score : " + str(player_score), (450, 135), 20, 'BlanchedAlmond')
    
    
# initialization frame
frame = simplegui.create_frame("Blackjack", 600, 600)
frame.set_canvas_background("Green")

#create buttons and canvas callback
frame.add_button("Deal", deal, 200)
frame.add_button("Hit",  hit, 200)
frame.add_button("Stand", stand, 200)
frame.set_draw_handler(draw)


# get things rolling
deal()
frame.start()


# remember to review the gradic rubric