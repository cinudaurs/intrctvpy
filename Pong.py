# Implementation of classic arcade game Pong 
  
import simplegui 
import random 
  
# initialize globals - pos and vel encode vertical info for paddles 
WIDTH = 600
HEIGHT = 400       
PAD_WIDTH = 8
PAD_HEIGHT = 80
HALF_PAD_WIDTH = PAD_WIDTH / 2
HALF_PAD_HEIGHT = PAD_HEIGHT / 2
LEFT = False
RIGHT = True
  
ball_pos = [WIDTH / 2, HEIGHT / 2] 
ball_vel = [0, 0] 

score1 = 0
score2 = 0




ball_radius = 20
  
paddle1_pos = [HALF_PAD_WIDTH , HEIGHT/2] 
paddle2_pos = [WIDTH - HALF_PAD_WIDTH, HEIGHT/2]
paddle1_vel = [0,0] 
paddle2_vel = [0,0] 
  
  
# initialize ball_pos and ball_vel for new bal in middle of table 
# if direction is RIGHT, the ball's velocity is upper right, else upper left 
def spawn_ball(direction): 
    global ball_pos, ball_vel # these are vectors stored as lists 
    
    ball_pos = [WIDTH / 2, HEIGHT / 2]
    
    
    if direction == False :
                     
        ball_vel[0] = - (int(random.randrange(120,240) / 60))
        ball_vel[1] = - (int(random.randrange(60,100) / 60))
        ball_pos[0] += ball_vel[0]
        ball_pos[1] += ball_vel[1]
        
    elif direction == True :
        
        ball_vel[0] =  (int(random.randrange(120,240) / 60))
        ball_vel[1] = - (int(random.randrange(60,100) / 60))
        ball_pos[0] += ball_vel[0]
        ball_pos[1] += ball_vel[1]
        
      
    
    
# define event handlers 
def new_game(): 
    global paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel  # these are numbers 
    global score1, score2  # these are ints 
    global LEFT, RIGHT
    spawn_ball(LEFT) 
  
def draw(canvas): 
    global score1, score2, paddle1_pos, paddle2_pos, ball_pos, ball_vel , ball_radius, RIGHT, LEFT
    global paddle1_vel, paddle2_vel
   
          
    # draw mid line and gutters 
    canvas.draw_line([WIDTH / 2, 0],[WIDTH / 2, HEIGHT], 1, "White") 
    canvas.draw_line([PAD_WIDTH, 0],[PAD_WIDTH, HEIGHT], 1, "White") 
    canvas.draw_line([WIDTH - PAD_WIDTH, 0],[WIDTH - PAD_WIDTH, HEIGHT], 1, "White") 
      
          
    # update ball 
      
    if ball_pos[1] <= ball_radius: 
            ball_vel[0] = ball_vel[0] 
            ball_vel[1] = -ball_vel[1] 
              
    elif ball_pos[1] >= (HEIGHT - 1) - ball_radius:     
            ball_vel[0] = ball_vel[0] 
            ball_vel[1] = -ball_vel[1] 
    
    elif ball_pos[0] <= PAD_WIDTH + ball_radius :
           
            if (ball_pos[1] <= paddle1_pos[1] + HALF_PAD_HEIGHT) and (ball_pos[1] >= paddle1_pos[1] - HALF_PAD_HEIGHT) :
                
                ball_vel[0] = -ball_vel[0] - (ball_vel[0] * 0.1) 
                ball_vel[1] = ball_vel[1]  + (ball_vel[0] * 0.1)  
            
            else:
                
                score2+=1
                spawn_ball(RIGHT)
    
    elif ball_pos[0] >= WIDTH - PAD_WIDTH - ball_radius :
            
            if (ball_pos[1] <= paddle2_pos[1] + HALF_PAD_HEIGHT) and (ball_pos[1] >= paddle2_pos[1] - HALF_PAD_HEIGHT) :
                
                ball_vel[0] = -ball_vel[0] - (ball_vel[0] * 0.1)
                ball_vel[1] = ball_vel[1] + (ball_vel[1] * 0.1)
            
            else:
                
                score1+=1
                spawn_ball(LEFT)
            
 
    ball_pos[0] += ball_vel[0] 
    ball_pos[1] += ball_vel[1] 
               
    # draw ball 
    canvas.draw_circle(ball_pos, ball_radius, 1, "Red", "Red") 
           
    # update paddle's vertical position, keep paddle on the screen 
      
    if paddle1_pos[1] < HALF_PAD_HEIGHT : 
         
        paddle1_pos[1] = HALF_PAD_HEIGHT    
        
                
    elif paddle1_pos[1] > (HEIGHT-1) - HALF_PAD_HEIGHT:
        
        paddle1_pos[1] = (HEIGHT-1) - HALF_PAD_HEIGHT
        
                    
    else:    
        paddle1_pos[1] += paddle1_vel[1]
    
    if paddle2_pos[1] < HALF_PAD_HEIGHT :
        
        paddle2_pos[1] = HALF_PAD_HEIGHT
        
              
    elif paddle2_pos[1] > (HEIGHT-1) - HALF_PAD_HEIGHT: 
            
        paddle2_pos[1] = (HEIGHT-1) - HALF_PAD_HEIGHT
        
                   
    else:
        paddle2_pos[1] += paddle2_vel[1]     
      
        
        
        
    # draw paddles 
    
    canvas.draw_line([paddle1_pos[0],paddle1_pos[1] - HALF_PAD_HEIGHT], [paddle1_pos[0], paddle1_pos[1] + HALF_PAD_HEIGHT], PAD_WIDTH, 'Orange')
    
    canvas.draw_line([paddle2_pos[0],paddle2_pos[1] - HALF_PAD_HEIGHT], [paddle2_pos[0], paddle2_pos[1] + HALF_PAD_HEIGHT], PAD_WIDTH, 'Aqua')
      
    # draw scores 
  
    canvas.draw_text(str(score1), (WIDTH/4, HALF_PAD_HEIGHT ), 30, 'Orange')
    
    canvas.draw_text(str(score2), ((3*WIDTH/4), HALF_PAD_HEIGHT ), 30, 'Aqua') 



def restart():
    global ball_pos, ball_vel, paddle1_pos, paddle2_pos, paddle1_vel, paddle2_vel
    global score1, score2
    
    score1 = 0
    score2 = 0
    ball_pos = [WIDTH / 2, HEIGHT / 2] 
    ball_vel = [0, 0]
    paddle1_pos = [HALF_PAD_WIDTH , HEIGHT/2] 
    paddle2_pos = [WIDTH - HALF_PAD_WIDTH, HEIGHT/2]
    paddle1_vel = [0,0] 
    paddle2_vel = [0,0]
    
    new_game()
    
    
    
    
          
def keydown(key): 
    global paddle1_vel, paddle2_vel 
    acc = 5
      
    if key == simplegui.KEY_MAP["s"]: 
        paddle1_vel[1] += acc 
    
    elif key == simplegui.KEY_MAP["w"]: 
        paddle1_vel[1] -= acc 
        
    elif key == simplegui.KEY_MAP["down"]:
        paddle2_vel[1] += acc
    
    elif key == simplegui.KEY_MAP["up"]:
        paddle2_vel[1] -= acc
          
  
    
def keyup(key): 
    global paddle1_vel, paddle2_vel 
    acc = 0
    
    if key==simplegui.KEY_MAP["s"]: 
        paddle1_vel[1] = acc 
    
    elif key==simplegui.KEY_MAP["w"]: 
        paddle1_vel[1] = acc 
     
    elif key == simplegui.KEY_MAP["down"]:
        paddle2_vel[1] = acc
    
    elif key == simplegui.KEY_MAP["up"]:
        paddle2_vel[1] = acc
        
    print paddle1_pos 
          
  
# create frame 
frame = simplegui.create_frame("Pong", WIDTH, HEIGHT) 
frame.set_draw_handler(draw) 
frame.set_keydown_handler(keydown) 
frame.set_keyup_handler(keyup)
frame.add_button("Restart", restart)
  
  
# start frame 
new_game() 
frame.start() 