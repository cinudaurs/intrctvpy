# Import modules
import simplegui
import random

# Global time tracker in units of tenth of a second
time_in_tenthofasecond = 0

# Time in string format for display purposes
str_time = '0:00.0'

width = 500
height = 500

# 0.1 sec interval for the event handler for the timer. 
interval = 100

stopwatch_position = [200,249]

# A global Boolean variable that is True,
# when the stopwatch is running.
# And False when the stopwatch is stopped.
started = False

# Counter to track the number of times 
# that you have stopped the watch. 
total_stops = 0

# Counter to track the number of times 
# that you managed to stop the watch 
# on a whole second (1.0, 2.0, 3.0, etc.)
successful_stops = 0

# Score is of the form 'x/y'. 
# where x is the number of successful stops 
# and y is number of total stops.
score = '0/0'

# It represents the position that these counters should be drawn in the 
# upper right-hand part of the stopwatch canvas in the form "x/y"
score_position = [425, 50]


# To format the time in the format : A:BC.D, for display.
def format(val):
    
    str_seconds = ''
    remainder = int(val)%10
    quotient = int(val)/10
    
    tenth_of_a_second = remainder
    seconds = (quotient)%60
    mins = (quotient)/60
    
    if seconds < 10 :
        str_seconds = "0"+str(seconds)
    else:
        str_seconds = str(seconds)
    
    return str(mins) + ":" + str_seconds + "."  + str(tenth_of_a_second)




# Handler for timer
def tick():
   global time_in_tenthofasecond
   global str_time
   
   time_in_tenthofasecond+=1
   str_time = format(time_in_tenthofasecond)
    
 
# Handler to draw on canvas
def draw(canvas):
    canvas.draw_text(str_time, stopwatch_position, 36, "White")
    canvas.draw_text(score, score_position, 25, "Orange")


  
# define event handlers for buttons; "Start", "Stop", "Reset"        


def stop_button():
    global time_in_tenthofasecond
    global started
    global total_stops
    global successful_stops
    global score
    
    if started == True:
        stopwatch_timer.stop()
        total_stops+=1
        
        remainder = int(time_in_tenthofasecond)%10
        quotient = int(time_in_tenthofasecond)/10
       
        tenth_of_a_second = remainder
        seconds = (quotient)%60
        
        if tenth_of_a_second == 0 and seconds > 0:
            successful_stops+=1
        
        score = str(successful_stops) + "/" + str(total_stops)
        started = False

def start_button():
    global started
    global time_in_tenthofasecond
        
    if started == False:
        stopwatch_timer.start()
        started  = True

def reset_button():
    global time_in_tenthofasecond
    global started
    global str_time
    global score
    global total_stops
    global successful_stops
    
    stopwatch_timer.stop()
    started = False
    time_in_tenthofasecond = 0
    str_time = '0:00.0'
    score = '0/0'
    total_stops = 0
    successful_stops = 0
    


# Create a frame 
frame = simplegui.create_frame("Home", width, height)

frame.add_button("Start", start_button, 75)
frame.add_button("Stop", stop_button, 75)
frame.add_button("Reset", reset_button, 75)


# Register event handlers
frame.set_draw_handler(draw)
stopwatch_timer = simplegui.create_timer(interval, tick)


# Start the frame animation
frame.start()



